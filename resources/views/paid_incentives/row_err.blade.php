<tr>
    <th width="2%"><input type="checkbox" /></th>
    <td>
        {{Form::select('category[]',$categoryList,null,
        ['class'=>'form-control','required'])}}
        @if($errors->has('category.'.$i))<span class="required text-danger">{{ 'Category '.substr(explode('.',$errors->first('category.'.$i))[1],2) }}</span>@endif
    </td>
    
    <td>
        {{Form::text('deposit_account[]',null,
        ['class'=>'form-control','required','placeholder'=>"Deposit Account"])}}
        @if($errors->has('deposit_account.'.$i))<span class="required text-danger">{{ 'Deposit Account '.substr(explode('.',$errors->first('deposit_account.'.$i))[1],2) }}</span>@endif
    </td>
    <td>
        {{Form::text('deposited_amt[]',null,
        ['class'=>'form-control','required','placeholder'=>"Deposited Amount"])}}
        @if($errors->has('deposited_amt.'.$i))<span class="required text-danger">{{ "Deposited Amount ".substr(explode('.',$errors->first('deposited_amt.'.$i))[1],2) }}</span>@endif
    </td>
    <td>
        {{Form::text('incentive_amt[]',null,
        ['class'=>'form-control','required','placeholder'=>"Incentive Amount"])}}
        @if($errors->has('incentive_amt.'.$i))<span class="required text-danger">{{ "Incentive Amount ".substr(explode('.',$errors->first('incentive_amt.'.$i))[1],2) }}</span>@endif
    </td>
    <td>
        {{Form::text('remarks[]',null,
        ['class'=>'form-control','required','placeholder'=>"Remarks"])}}
        @if($errors->has('remarks.'.$i))<span class="required text-danger">{{ "Remarks ".substr(explode('.',$errors->first('remarks.'.$i))[1],2) }}</span>@endif
    </td>
</tr>