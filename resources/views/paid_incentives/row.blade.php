<tr>
    <th width="2%"><input type="checkbox" /></th>
    <td>
        {{Form::select('category[]',$categoryList,null,
        ['class'=>'form-control','required'])}}
        @if($errors->has('category'))<span class="required text-danger">{{ $errors->first('category') }}</span>@endif
    </td>
    
    <td>
        {{Form::text('deposit_account[]',null,
        ['class'=>'form-control','required','placeholder'=>"Deposit Account"])}}
        @if($errors->has('deposit_account'))<span class="required text-danger">{{ $errors->first('deposit_account') }}</span>@endif
    </td>
    <td>
        {{Form::text('deposited_amt[]',null,
        ['class'=>'form-control','required','placeholder'=>"Deposited Amount"])}}
        @if($errors->has('deposited_amt'))<span class="required text-danger">{{ $errors->first('deposited_amt') }}</span>@endif
    </td>
    <td>
        {{Form::text('incentive_amt[]',null,
        ['class'=>'form-control','required','placeholder'=>"Incentive Amount"])}}
        @if($errors->has('incentive_amt'))<span class="required text-danger">{{ $errors->first('incentive_amt') }}</span>@endif
    </td>
    <td>
        {{Form::text('remarks[]',null,
        ['class'=>'form-control','required','placeholder'=>"Remarks"])}}
        @if($errors->has('remarks'))<span class="required text-danger">{{ $errors->first('remarks') }}</span>@endif
    </td>
</tr>